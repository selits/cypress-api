/// <reference types="cypress" />
const faker = require('faker');
let randomUsername = faker.name.firstName();
let randomNumber = faker.random.number();


describe('/Account/v1/User Test Suite', () => {
  context('POST request method', () => {
    it('should return 201 when new user creation is success', () => {
      cy.request({
        method: 'POST',
        url: '/Account/v1/User',
        form: true,
        body: {
          userName: `${randomUsername} + ${randomNumber}`,
          password: `Pass + ${randomNumber} + !`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(201);
      });
    });
  
    it('should returned 406 when User Name exist', () => {
      cy.request({
        method: 'POST',
        url: '/Account/v1/User',
        form: true,
        failOnStatusCode: false,
        body: {
          userName: `JohnDoe`,
          password: `Jd12345678!`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(406);
      });
    });
  
    it('should return 400 when the password creation is incorrect', () => {
      cy.request({
        method: 'POST',
        url: '/Account/v1/User',
        form: true,
        failOnStatusCode: false,
        body: {
          userName: `${randomUsername} + ${randomNumber}`,
          password: `${randomNumber}`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(400);
      });
    });
  });

  context('DELETE', () => {
    it('should return 404 when method request is wrong', () => {
      cy.request({
        method: 'DELETE',
        url: '/Account/v1/User',
        form: true,
        failOnStatusCode: false,
        body: {
          userName: `${randomUsername} + ${randomNumber}`,
          password: `Pass + ${randomNumber} + !`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(404);
      });
    });
  })

  context('PUT', () => {
    it('should return 404 when method request is wrong', () => {
      cy.request({
        method: 'PUT',
        url: '/Account/v1/User',
        form: true,
        failOnStatusCode: false,
        body: {
          userName: `${randomUsername} + ${randomNumber}`,
          password: `Pass + ${randomNumber} + !`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(404);
      });
    });
  })

  context('GET', () => {
    it('should return 200 when grabbing existing user info', () => {
      cy.request({
        method: 'GET',
        url: '/Account/v1/User',
        form: true,
        failOnStatusCode: false,
        body: {
          userName: `JohnDoe`,
          password: `Jd12345678!`
        }
      })
      .then((response) => {
        expect(response.status).to.eq(200);
      });
    });
  })
});